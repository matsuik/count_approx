def copy_param(target_link, source_link, only_head=False):
    """Copy parameters of a link to another link.
    """
    if not only_head:
        target_params = dict(target_link.namedparams())
        for param_name, param in source_link.namedparams():
            target_params[param_name].data[:] = param.data
    else:
        target_params = dict(target_link.namedparams())
        for param_name, param in source_link.namedparams():
            if param_name.split('/')[1] == '0':
                target_params[param_name].data[:] = param.data


def copy_grad(target_link, source_link):
    """Copy gradients of a link to another link.
    """
    target_params = dict(target_link.namedparams())
    for param_name, param in source_link.namedparams():
        target_params[param_name].grad[:] = param.grad
