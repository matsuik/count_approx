# neural netの構造
## head
dqn_head.py
## tale
policy.py
v_function.py

# train_loop
a3c_ale.train_loop

# 1stepの学習
a3c.A3C.run

# TODO
## count用のtale
- policy.py, v_fuction.pyとおなじのをつくる
- outputのバイアスを外から与えられるようにする
- a3c_ale.A3CFFなどと同じクラスを使う
- それをa3c_ale.model_optから呼ぶ

## Rn+の計算
- for i in reverse ...:の中でやる
- 普通のrewardとの相対重みbetaを外から与えられる用にする

## 勾配計算
- a3c.A3C.runの下の方でやる
- unchainするの面倒ければ、total_lossに加えるだけでよい

## countのlogging
a3c.A3C.runの中でやろう

## unchainするなら
a3c_ale.A3CLSTM.unchain_backwardみたいにやる
